///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file eval.cpp
/// @version 1.0
///
/// Evaluate a number of Standard C++ Collection classes for performance
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   11_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <cstring>
#include <string>
#include <cmath>
#include <x86intrin.h>
#include <limits.h>

using namespace std;

typedef unsigned long ticks_t;

constexpr unsigned int TESTS              = 5;    // The number of elements in TEST_SIZE[]
constexpr unsigned int TEST_SIZE[TESTS]   = { 10, 100, 1000, 10000, 100000 };
constexpr unsigned int THROWAWAY_RUNS     = 2;    // Unused runs that initialize memory
constexpr unsigned int TEST_RUNS          = 10;   // Measured test runs
constexpr unsigned int SEARCH_TESTS       = 100;  // Number of times we try searching the data structure
constexpr unsigned int CALIBRATE_OVERHEAD = 1000; // Number of times to calibrate the test overhead
// #define PRINT_PROGRESS

// Inline assembly routine that returns high percision monotonic clock
// ticks from the CPU
#define MARK_TIME(ticks)  asm inline volatile (               \
                          "RDTSCP\n\t"                        \
                          "SHL $32,   %%rdx\n\t"              \
                          "OR  %%rax, %%rdx\n\t"              \
                          "MOV %%rdx, %0\n\t"                 \
                          : "=r" (ticks)                      \
                          :                                   \
                          : "%rax", "%rcx", "%rdx")           ;

long TEST_OVERHEAD = LONG_MAX;

// Compute the number of ticks between start and end, subtracting the overhead.
// If it's negative, then it's 0.
#define DIFF_TIME(start, end) ((end-start-TEST_OVERHEAD > 0) ? (end-start-TEST_OVERHEAD) : 0)

/// This abstract test class is a template for concrete classes that can
/// actually do tests.
class ABSTRACT_TEST_CLASS {
protected:
	static long start, end;
	static int  insert_value;
	static int  search_value;

public:
	virtual inline void    initDataStructure()  __attribute__((always_inline)) = 0;
	virtual inline ticks_t testInsert()         __attribute__((always_inline)) = 0;
	virtual inline ticks_t clearDataStructure() __attribute__((always_inline)) = 0;
	virtual inline ticks_t testSearch()         __attribute__((always_inline)) = 0;
};

long ABSTRACT_TEST_CLASS::start;
long ABSTRACT_TEST_CLASS::end;
int  ABSTRACT_TEST_CLASS::insert_value;
int  ABSTRACT_TEST_CLASS::search_value;


//////////////////////////////////  Vector  //////////////////////////////////
///
/// This class wraps a vector<long> container and has methods to:
///   - Initialize the vector
///   - Insert data into the vector (measuring the insertion time)
///   - Clear data out of the vector
///   - Search for data in the vector
class TestVector : public ABSTRACT_TEST_CLASS {
private:
	static vector<long> container;

public:
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		auto endIterator = container.end();
		insert_value = rand();

		MARK_TIME( start );

		// Do the operation ///////
		container.insert( endIterator, insert_value );
		///////////////////////////

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		MARK_TIME( start );

		container.clear();

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );

		for( auto i : container ) {
			if( i == search_value )
				break;
		}

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

}; // Vector
vector<long> TestVector::container;


//////////////////////////////////  List  //////////////////////////////////
///
/// This class wraps a list<long> container.
/// Since list and vector's function calls are identically named,
/// TestList can directly inherit from TestVector.
class TestList : public TestVector {
private:
   //differentiate the container variable as a list instead of a vector
	static list<long> container;
}; // List
list<long> TestList::container;


//////////////////////////////////  Set  //////////////////////////////////
///
/// This class wraps a set<long> container and has methods to:
///   - Search for data in the vector
/// The remaining methods are inherited from TestVector.
class TestSet : public TestVector {
private:
	static set<long> container;

public:
   //edited
	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );

      container.contains(search_value);

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}
}; // Set
set<long> TestSet::container;


//////////////////////////////////  Map  //////////////////////////////////
///
/// This class wraps a map<long, long> container.
/// Since map and set's function calls are identically named,
/// TestMap can directly inherit from TestSet.
class TestMap : public TestSet {
private:
	static map<long, long> container;
}; // Map
map<long, long> TestMap::container;


//////////////////////////////////  Unordered Map  //////////////////////////////////
///
/// This class wraps an unordered_map<long, long> container.
/// Since unordered_map and set's function calls are identically named,
/// TestUnorderedMap can directly inherit from TestSet.
class TestUnorderedMap : public TestSet {
private:
	static unordered_map<long, long> container;
}; // Unordered Map
unordered_map<long, long> TestUnorderedMap::container;


//////////////////////////////////  Unordered Set  //////////////////////////////////
///
/// This class wraps an unordered_set<long, long> container.
/// Since unordered_set and set's function calls are identically named,
/// TestUnorderedSet can directly inherit from TestSet.
class TestUnorderedSet : public TestSet {
private:
	static unordered_set<long, long> container;
}; // Unordered Set
unordered_set<long, long> TestUnorderedSet::container;


//////////////////////////////////  Result  //////////////////////////////////
class Result {
private:
	ABSTRACT_TEST_CLASS* pTestClass = nullptr;

	string structure = "";
	ticks_t insert_ticks[TESTS];                          // Sum of all ticks
	ticks_t clear_ticks[TESTS];                           // Sum of all ticks
	ticks_t search_ticks[TESTS];                          // Sum of search ticks

public:
	Result( ABSTRACT_TEST_CLASS* pNewTestClass, string_view newStructureName ) {
		clear();
		pTestClass = pNewTestClass;
		structure  = newStructureName;
	}

	const string_view getStructure() const { return structure; }

	void clear() {
		structure = "";
		memset( insert_ticks, 0, sizeof( insert_ticks) );
		memset( clear_ticks,  0, sizeof( clear_ticks) );
		memset( search_ticks, 0, sizeof( search_ticks) );
	}

	void recordInsertTicks( size_t testIndex, size_t runIndex, ticks_t result ) {
		if( runIndex >= THROWAWAY_RUNS ) {
			insert_ticks[testIndex]  += result;
		}
	}

	void recordClearTicks( size_t testIndex, size_t runIndex, ticks_t result ) {
		if( runIndex >= THROWAWAY_RUNS ) {
			clear_ticks[testIndex]  += result;
		}
	}

	void recordSearchTicks( size_t testIndex, size_t runIndex, ticks_t result ) {
		if( runIndex >= THROWAWAY_RUNS ) {
			search_ticks[testIndex]  += result;
		}
	}

	static void printResultsHeader() {
		printf( "%21s", " " );
		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			printf( "%13u ", TEST_SIZE[test] );
		}
		printf( "\n" );

		printf( "==================== " );
		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			printf( "============= " );
		}
		printf( "\n" );
	}

	void printResults() const {
		printf( "%-20s", (structure + " insert").c_str() );

		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			ticks_t average = insert_ticks[test] / ( TEST_RUNS * TEST_SIZE[test] );
			printf( "%14lu", average );
		}
		printf( "\n" );

		printf( "%-20s", (structure + " search").c_str() );

		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			ticks_t average = search_ticks[test] / ( TEST_RUNS * SEARCH_TESTS );
			printf( "%14lu", average );
		}
		printf( "\n" );


		printf( "%-20s", (structure + " clear").c_str() );

		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			ticks_t average = clear_ticks[test] / ( TEST_RUNS );
			printf( "%14lu", average );
		}
		printf( "\n" );

		printf( "- - - - - - - - - - \n" );
	}

	void testDataStructure() {
		for( size_t testIndex = 0 ; testIndex < TESTS ; testIndex++ ) {
			unsigned int testSize = TEST_SIZE[testIndex];  // Store locally, so we aren't always computing it

			for( size_t run = 0 ; run < THROWAWAY_RUNS + TEST_RUNS ; run++ ) {
				// Setup the test infrastructure
				ticks_t accumulator = 0;

				// Setup the test //////
				////////////////////////
				pTestClass->initDataStructure();

				// Insert the data
				for( size_t loop = 0 ; loop < testSize ; loop++ ) {
					accumulator += pTestClass->testInsert();
				}

				// Record the inserts
				recordInsertTicks( testIndex, run, accumulator );

				// Search the data
				accumulator = 0;
				for( size_t loop = 0 ; loop < SEARCH_TESTS ; loop++ ) {
					accumulator += pTestClass->testSearch();
				}
				recordSearchTicks( testIndex, run, accumulator );

				// Clear the data structure and record the results
				recordClearTicks( testIndex, run, pTestClass->clearDataStructure() );

#ifdef PRINT_PROGRESS
				// Print progress
				cout << ".";
				cout.flush();
#endif
			} // testIndex : TESTS

#ifdef PRINT_PROGRESS
		// Print progress
		cout << " >> ";
		cout.flush();
#endif

		} // run : RUNS
	} // testDataStructure()

} ; // Result


///////////////////////////////////  main  ///////////////////////////////////
int main() {
	cout << "Welcome to the Gnu C++ Collection Class Evaluator" << endl;

	static long start, end;
	for( unsigned int i = 0 ; i < CALIBRATE_OVERHEAD ; i++ ) {
		MARK_TIME( start );
		MARK_TIME( end );
		TEST_OVERHEAD = min( TEST_OVERHEAD, (end - start) );
	}

	cout << "Approximate test overhead is: " << TEST_OVERHEAD << endl;

	// Implement your Collection Class Evaluator here
   Result::printResultsHeader();

   TestVector testVector;
   Result vectorResult( &testVector, "vector" );
   vectorResult.testDataStructure();
   vectorResult.printResults();

   TestList testList;
   Result listResult( &testList, "list" );
   listResult.testDataStructure();
   listResult.printResults();

   TestSet testSet;
   Result setResult ( &testSet, "set" );
   setResult.testDataStructure();
   setResult.printResults();

   TestMap testMap;
   Result mapResult ( &testMap, "map" );
   mapResult.testDataStructure();
   mapResult.printResults();

   TestUnorderedMap testUnorderedMap;
   Result unorderedMapResult ( &testUnorderedMap, "unordered map" );
   unorderedMapResult.testDataStructure();
   unorderedMapResult.printResults();

   TestUnorderedSet testUnorderedSet;
   Result unorderedSetResult ( &testUnorderedSet, "unordered set" );
   unorderedSetResult.testDataStructure();
   unorderedSetResult.printResults();

} // main()
